FROM node:14

WORKDIR /app

COPY package*.json ./

RUN apt-get update && apt-get install python3-distutils -y
RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python3 get-pip.py
RUN pip install asyncio
RUN pip install python-telegram-bot

RUN curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc | tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && echo "deb https://ngrok-agent.s3.amazonaws.com buster main" | tee /etc/apt/sources.list.d/ngrok.list && apt update && apt install ngrok

RUN npm install --only=production

COPY . .

RUN npm install react-scripts

RUN npm install ngrok

RUN npm run build

EXPOSE 3000

CMD ["npm", "run", "serve"]

