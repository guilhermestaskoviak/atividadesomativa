import React, { useState } from 'react';
import './App.css';

function Login() {
  const [email, setEmail] = useState('');
  const [senha, setSenha] = useState('');
  const [mensagem, setMensagem] = useState('');

  const handleSubmit = (verf) => {
    verf.preventDefault();
    if (email === 'guilhermestaskoviak@gmail.com' && senha === '123456') {
      setMensagem('Acessado com sucesso!');
    } else {
      setMensagem('Usuario ou senha incorreto!');
    }
  };

  return (
    <div className="login">
      <h1 className="titulo">Faça login</h1>
      <form className="formulario" onSubmit={handleSubmit}>
        <label className="label">
          Email:
          <input className="input" type="email" name="email" value={email} onChange={(e) => setEmail(e.target.value)} />
        </label>
        <label className="label">
          Senha:
          <input className="input" type="password" name="senha" value={senha} onChange={(e) => setSenha(e.target.value)} />
        </label>
        <button className="botao" type="submit">Acessar</button>
      </form>
      {mensagem && <p className="mensagem">{mensagem}</p>}
    </div>
  );
}

function App() {
  return (
    <div className="App">
      <Login />
    </div>
  );
}

export default App;

